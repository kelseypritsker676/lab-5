/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MyClinic;
import MyUI.Visit;

/**
 *
 * @author kelsey.pritsker676
 */
public class ShowVisits {
    Visit vis = new Visit();
    
    public ShowVisits(Visit currVis){
        vis = currVis;
        display();
    }
    
    public void display(){
        double pay = vis.getVisitPay();
        System.out.println("Payment for visit: $" + pay);
    }
}

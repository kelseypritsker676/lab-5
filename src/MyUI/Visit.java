/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package MyUI;

/**
 *
 * @author kelsey.pritsker676
 */
public class Visit {
    double visitPay;
    
    public Visit(){
        visitPay = 0;
    }
    
    public void setVisitPay(double newVisitPay){
        visitPay = newVisitPay;
    }
    
    public double getVisitPay(){
        return visitPay;
    }
}
